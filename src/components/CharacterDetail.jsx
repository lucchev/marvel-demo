import * as React from 'react';
//
import {Character} from '../types/';

class CharacterDetail extends React.Component {

    static propTypes = {
        data : Character
    }

    render () {
        const { data } = this.props;
        return (
            <div>
            <h1>Fiche identité :</h1>
            <div className="card-deck">
                <div className="card mb-4">
                <img className="card-img-top img-fluid" src={`${data.thumbnail.path}.${data.thumbnail.extension}`} alt="Character Card" />
                <div className="card-body">
                    <h5 className="card-title">{data.name}</h5>
                    <p className="card-text">{data.description}</p>
                </div>
            </div>
          </div>
        </div>   
        );
    }
}

export default CharacterDetail;