import * as React from 'react';
import CharacterItem from './CharacterItem';
import PropTypes from 'prop-types';
//
import {Character} from '../types/';

class CharacterList extends React.Component {

  static propTypes = {
    data: PropTypes.arrayOf(Character),
    onClick: PropTypes.func.isRequired
  }
  
  static defaultProps =  {
    data : []
  }

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(characterId) {
    this.props.onClick(characterId);
  }

  render() {
    const { data } = this.props;
    let characters = data.map((character) => <div className="col-md-4">
            <CharacterItem
              key={character.id}
              data={character}
              onClick={this.handleClick}
            />
          </div>
    );



    return (
        <div>
          <h1>Liste des super héros :</h1>
          <div className="row">
            {characters}
          </div>
        </div>
    );
  }
}

export default CharacterList;