import * as React from 'react';
import PropTypes from 'prop-types';
//
import {Character} from '../types/';

class CharacterItem extends React.Component {

    static propTypes = {
        data: Character,
        onClick: PropTypes.func.isRequired
    }

  constructor(props) {
      super(props);
      this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onClick(this.props.data.id);
  }

  render () {
      const { data } = this.props;
      return (
        <div className="card mb-4">
            <img className="card-img-top img-fluid" src={`${data.thumbnail.path}.${data.thumbnail.extension}`} alt="Character Card" />
            <div className="card-body">
                <h5 className="card-title">{data.name}</h5>
                
                <button type="button" className="btn btn-default" onClick={this.handleClick}>Details</button>
            </div>
        </div>
      );
  }
}

export default CharacterItem;