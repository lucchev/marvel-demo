import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
//
import * as Actions from '../actions/CharacterActions';
import {Character} from '../types/';
import CharacterList from '../components/CharacterList';
import CharacterDetail from '../components/CharacterDetail';

class App extends React.Component {
    static propTypes = {
        characters: PropTypes.arrayOf(Character),
        characterInfo: Character,
        actions: PropTypes.object.isRequired
    };
    
    static defaultProps = {
        characters: []
    };

    constructor(props) {
        super(props);
        this.handleInfo= this.handleInfo.bind(this);
    }


    componentWillMount() {
        this.props.actions.fetchCharacters();
    }
    
    componentDidUpdate() {
      window.scrollTo(0, 0);
    }

    handleInfo(characterId) {
        this.props.actions.showInfo(characterId);
    }

    render() {
        const {characters, characterInfo} = this.props;

        const characterList = <CharacterList data={characters} onClick={this.handleInfo} />
        const characterDetail = characterInfo && <CharacterDetail data={characterInfo} />;

        return (
          <div className="container">
            {characterDetail}
            {characterList}
        </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { characters, characterInfo } = state;
    return {
        characters, 
        characterInfo
    };
}

function mapDispatchToProps(dispatch) {
    const actionMap = {
        actions: bindActionCreators(Actions, dispatch)
    };

    return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
