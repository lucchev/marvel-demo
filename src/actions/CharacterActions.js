import * as Api from '../api/MarvelAPI';
import {
  FETCH_CHARACTERS,
  FETCH_CHARACTERS_SUCCESS,
  FETCH_CHARACTERS_ERROR,
  FETCH_CHARACTERS_INFO,
  FETCH_CHARACTERS_INFO_SUCCESS,
  FETCH_CHARACTERS_INFO_ERROR,
  SHOW_INFO
} from '../constants';

export function showInfo(characterId) {
    return (dispatch) => {
      dispatch({
          type: SHOW_INFO,
          payload: {characterId}
      });

      return fetchCharactersInfo(characterId, dispatch);
    };
}

export function fetchCharacters() {
    return (dispatch) => {
        dispatch({
            type: FETCH_CHARACTERS,
            payload: {}
        });

        return Api.fetchCharacters()
        .then(responseData => {
            dispatch(fetchCharactersSuccess(responseData));
        })
        .catch((error) => {
            dispatch(fetchCharactersError(error));
        });
    }
}

function fetchCharactersSuccess(response) {
    return {
          type: FETCH_CHARACTERS_SUCCESS,
          payload: {response}
      };
}

function fetchCharactersError(error) {
    return {
          type: FETCH_CHARACTERS_ERROR,
          payload: {error}
      };
}

function fetchCharactersInfo(characterId, dispatch) {
    dispatch({
        type: FETCH_CHARACTERS_INFO,
        payload: {}
    });

    return Api.fetchCharactersInfo(characterId)
    .then(responseData => {
        dispatch(fetchCharactersInfoSuccess(responseData));
    })
    .catch((error) => {
        dispatch(fetchCharactersInfoError(error));
    });
}

function fetchCharactersInfoSuccess(response) {
  return {
        type: FETCH_CHARACTERS_INFO_SUCCESS,
        payload: {response}
    };
}

function fetchCharactersInfoError(error) {
  return {
        type: FETCH_CHARACTERS_INFO_ERROR,
        payload: {error}
    };
}
