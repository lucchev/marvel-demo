export const  FETCH_CHARACTERS  = 'FETCH_CHARACTERS';
export const  FETCH_CHARACTERS_SUCCESS = 'FETCH_CHARACTERS_SUCCESS';
export const  FETCH_CHARACTERS_ERROR = 'FETCH_CHARACTERS_ERROR';
export const  SHOW_INFO = 'SHOW_INFO';
export const  FETCH_CHARACTERS_INFO  = 'FETCH_CHARACTERS_INFO';
export const  FETCH_CHARACTERS_INFO_SUCCESS = 'FETCH_CHARACTERS_INFO_SUCCESS';
export const  FETCH_CHARACTERS_INFO_ERROR = 'FETCH_CHARACTERS_INFO_ERROR';
