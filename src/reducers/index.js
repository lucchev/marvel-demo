import { combineReducers } from 'redux';
import {
  FETCH_CHARACTERS_SUCCESS,
  FETCH_CHARACTERS_ERROR,
  FETCH_CHARACTERS_INFO_SUCCESS,
  FETCH_CHARACTERS_INFO_ERROR
} from '../constants';

const characters = (state, action) => {
  console.log('reducer', action);
  switch (action.type) {
    case FETCH_CHARACTERS_SUCCESS:
      return action.payload.response.data.results;
    case FETCH_CHARACTERS_ERROR:
        return [];
    default:
      return state || [];
  }
}

const characterInfo = (state, action) => {
  switch (action.type) {
    case FETCH_CHARACTERS_INFO_SUCCESS:
      return action.payload.response.data.results[0];
    case FETCH_CHARACTERS_INFO_ERROR:
        return {};
    default:
      return state || null;
  }
}

const rootReducer = combineReducers({
  characters,
  characterInfo
});

export default rootReducer;