import PropTypes from 'prop-types';

export const Url = PropTypes.shape({
    type: PropTypes.string,
    url: PropTypes.string
});

export const Image = PropTypes.shape({
    path: PropTypes.string,
    extension: PropTypes.string
});

export const ResourceList = PropTypes.shape({
    available: PropTypes.number,
    returned: PropTypes.number,
    collectionURI: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.any)
});

export const Character = PropTypes.shape({
    id : PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string,
    modified: PropTypes.string,
    resourceURI: PropTypes.string,
    urls: PropTypes.arrayOf(Url),
    thumbnail: Image,
    comics: ResourceList,
    stories: ResourceList,
    events: ResourceList,
    series: ResourceList
});