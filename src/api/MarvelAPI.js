import 'whatwg-fetch';
import crypto from 'crypto';

const API_PUBLIC = "298bab46381a6daaaee19aa5c8cafea5";
const API_PRIVATE = "b0223681fced28de0fe97e6b9cd091dd36a5b71d";
const MarvelURL = "http://gateway.marvel.com";

const getTS = () => Math.floor(Date.now());
const getHash = (ts) => crypto.createHash('md5').update(`${ts}${API_PRIVATE}${API_PUBLIC}`).digest('hex');

const fetchDatas = (endPoint, queryParams) => {
    console.log('fetchDatas', `${MarvelURL}/${endPoint}?${queryParams}`);
    return fetch(`${MarvelURL}/${endPoint}?${queryParams}`)
    .then((response) => {
        if (response.status >= 400) {
            throw new Error("Bad response from server");
        }
        return response.json();
    })
    .then((responseData) => {
        return responseData;
    })
    .catch(error => {
      throw error;
    });
}

const getQueryParams = () => {
    const ts = getTS();
    const hash = getHash(ts);
    return `ts=${ts}&apikey=${API_PUBLIC}&hash=${hash}`;
}

export const fetchCharacters = () => {
    const endPoint = "v1/public/characters";
    const queryParams = getQueryParams();
    return fetchDatas(endPoint, queryParams);
}

export const fetchCharactersInfo = (characterId) => {
    const endPoint = `v1/public/characters/${characterId}`;
    const queryParams = getQueryParams();
    return fetchDatas(endPoint, queryParams);
}
